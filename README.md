# chord_project_service

```json
{
  "id": "project",
  "repository": "https://bitbucket.org/genap/chord_project_service",
  "data_service": false,
  "apt_dependencies": [],
  "wsgi": true,
  "python_module": "chord_project_service.app",
  "python_callable": "application",
  "python_environment": {
    "DATABASE": "{SERVICE_DATA}/project_service.db"
  }
}
```
